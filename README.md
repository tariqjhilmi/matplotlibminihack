# November Monthly Mini Hack - Creating that perfect graph using matplotlib
Jupyter notebooks from the November Monthly Mini Hack where [Tariq Hilmi](https://gitlab.com/tariqjhilmi) presented an interactive workshop on creating and modifying figures using Python plotting library matplotlib.

#### [Slides](https://f1000research.com/slides/11-1399) | [Video](https://osf.io/389xw)

![Poster](images/poster.png)

Monthly Mini Hacks are monthly workshops to help you with those tech skills you always wanted to get but were too afraid to ask for!

They are organised by the [**Mini Hack Consortium**](https://osf.io/wbupr/), a multi-institution partnership that supports the intersection between open science and coding. We aim to cultivate best programming practices in research and to provide peer-to-peer support for learning new skills and abilities.

If you are interested in attending or presenting, contact us on [Twitter](https://twitter.com/MHacksC) or follow us on [eventbrite](https://www.eventbrite.co.uk/cc/monthly-mini-hacks-440659)
